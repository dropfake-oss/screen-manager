#nullable enable

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using ScreenManagement;

[CustomEditor(typeof(ScreenManager))]
public class ScreenManagerEditor : UnityEditor.Editor
{
	private ScreenManager _screenManager;

	private VisualTreeAsset VisualTree;

	private PropertyField? _useVariablePanelValuesProperty;
	private VisualElement? _allPanelValuesContainer;
	private SerializedProperty? _boolShowPanelValuesProperty;

	private PropertyField? _showDebugStackProperty;
	private SerializedProperty? _boolShowDebugStackProperty;

	public void OnEnable()
	{
		_screenManager = (ScreenManager)target;
		VisualTree = _screenManager.CustomInspectorXML;

		_boolShowPanelValuesProperty = serializedObject.FindProperty("UseVariablePanelValues");
		_boolShowDebugStackProperty = serializedObject.FindProperty("ShowDebugStack");
	}

	public override VisualElement CreateInspectorGUI()
	{
		//return base.CreateInspectorGUI();

		// Create a new GUI and copy all the elements in from provided Visual Tree
		VisualElement root = new VisualElement();
		VisualTree.CloneTree(root);

		_useVariablePanelValuesProperty = root.Q<PropertyField>("PropertyUseVariablePanelValues");
		_useVariablePanelValuesProperty.RegisterCallback<ChangeEvent<bool>>(OnUseVariablePanelValuesBoolChanged);
		_allPanelValuesContainer = root.Q<VisualElement>("AllPanelValues");

		_showDebugStackProperty = root.Q<PropertyField>("PropertyShowDebugStack");
		_showDebugStackProperty.RegisterCallback<ChangeEvent<bool>>(OnShowDebugStackBoolChanged);

		CheckDisplayPanelValues();
		CheckForPlayerPrefs();
		UpdateShowDebugStackPreference();

		return root;
	}

	private void OnUseVariablePanelValuesBoolChanged(ChangeEvent<bool> evt)
	{
		CheckDisplayPanelValues();
	}

	private void CheckDisplayPanelValues()
	{
		if (_allPanelValuesContainer == null)
		{
			Debug.LogError("SM Editor - Unable to find panel values container. Aborting CheckForDisplayType()...");
			return;
		}

		if (_boolShowPanelValuesProperty != null)
		{
			if (_boolShowPanelValuesProperty.boolValue)
			{
				_allPanelValuesContainer.style.display = DisplayStyle.Flex;
			}
			else
			{
				_allPanelValuesContainer.style.display = DisplayStyle.None;
			}
		}
	}

	private void OnShowDebugStackBoolChanged(ChangeEvent<bool> evt)
	{
		UpdateShowDebugStackPreference();
	}

	// checks to see if the preference was changed during Play mode
	private void CheckForPlayerPrefs()
	{
		if (PlayerPrefs.HasKey(_screenManager.DEBUG_SAVE_KEY))
		{
			if (_boolShowDebugStackProperty != null)
			{
				_boolShowDebugStackProperty.boolValue = PlayerPrefs.GetInt(_screenManager.DEBUG_SAVE_KEY) == 1 ? true : false;
			}
			
		}
	}

	// handles updating player prefs OUTSIDE of Play mode
	private void UpdateShowDebugStackPreference()
	{
		if (_boolShowDebugStackProperty != null)
		{
			if (_boolShowDebugStackProperty.boolValue)
			{
				PlayerPrefs.SetInt(_screenManager.DEBUG_SAVE_KEY, 1);
				_screenManager.ToggleDebugView(true);
			}
			else
			{
				PlayerPrefs.SetInt(_screenManager.DEBUG_SAVE_KEY, 0);
				_screenManager.ToggleDebugView(false);
			}
		}
	}
}
