using System;

namespace ScreenManagement
{
	public class ScreenData
	{
		public enum PriorityType
		{
			Immediate,
			Queue,
			FrontOfQueue
		}

		public string Id;
		public bool ClickBlockerToDismiss = true;
		public bool ShowTransitionIn = true;
		public bool ShowTransitionOut = true;
		public bool IsOverlay = false;
		public bool IncludeBlockingScrim = true;
		public bool IgnoreDuplicates = true;
		public string BgType = BackgroundType.DEFAULT;
		public bool BlurBackground = false;
		/* note: headertitle and headerstate could be combined as headerData, but i want to allow title updates
		without state changes and providing headerdata suggests that both will be provided. we can evaluate this as
		our needs change in the future */
		public string HeaderTitle = "";
		public string? HeaderState = null;
		public bool PreserveOnClose = false;
		public Action? ShowCallback = null;
		public Action? HideCallback = null;
		public Action? OnBackOverride = null;

		public PriorityType priority = PriorityType.Immediate;

		public ScreenData(string id)
		{
			Id = id;
		}

		public ScreenData(string id, bool blockerDismiss = true)
		{
			Id = id;
			ClickBlockerToDismiss = blockerDismiss;
		}

		/// <summary>
		/// Called when creating screen via debug menu.
		/// Initialize fields in DummyData(), as
		/// required in this clas or subclasses.
		/// </summary>
		public virtual void DummyData()
		{
			//Initialize the field with test values here...
		}
	}

	public class ScreenDataWithDetails : ScreenData
	{
		public string Title;
		public string Description;

		public ScreenDataWithDetails(string id, string title, string description): base(id)
		{
			Id = id;
			Title = title;
			Description = description;
		}
	}
}
