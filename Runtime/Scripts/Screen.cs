﻿using UI.Core;
using UnityEngine.UIElements;
using UnityEngine;
using System;
using DG.Tweening;
using Addressable.Utils;

namespace ScreenManagement
{
	public class ScreenBase : VisualElementWithSubscriptions
	{
		public virtual void Init(ScreenData data) { }

		public virtual void OnInit() { }

		public bool CloseOnBlockingScrimClick { get; protected set; }
		public bool IsOverlay { get; protected set; }
		public bool TransitionIn { get; protected set; }    //handle this in Screen so we don't have to transpose these values??
		public bool TransitionOut { get; protected set; }   //handle this in Screen so we don't have to transpose these values??
		protected bool IsBuilt;

		public VisualElement? BlockingScrim;

		public IAssetReferenceHandler? AssetReferenceHandler;
		public string Id { get; protected set; } = null!;

		public virtual void Show(Action? nCallback = null, ScreenDirection? direction = null)
		{
			var finalPosition = 0; // will 0 always be what we want?? can't depend on initial value if we're tweening multiple times
								   //Debug.Log($"Transition In Start-> from: {Screen.height} to: {finalPosition}");

			float ScreenTransitionTime = 0.6F;

			DOTween.To(() => Screen.height, x => style.top = x, finalPosition, ScreenTransitionTime)
				.SetEase(Ease.OutBack)
				.OnComplete(() =>
				{
					nCallback?.Invoke();
				});
		}

		public virtual void Hide(Action? nCallback = null, ScreenDirection? direction = null)
		{
			var initialValue = style.top.value.value;
			float ScreenTransitionTime = 0.6F;

			DOTween.To(() => initialValue, x => style.top = x, Screen.height, ScreenTransitionTime)
				.SetEase(Ease.InBack)
				.OnComplete(() =>
				{
					nCallback?.Invoke();
				});
		}

		//can be used to externally trigger next in a sequence or chain (or however a particular screen wants to handle it)
		public virtual void Next()
		{
			Debug.Log("Current screen does not implement Next");
		}

		public virtual void Prev()
		{
			Debug.Log("Current screen does not implement Prev");
		}

		public virtual void GoTo(int index)
		{
			Debug.Log("Current screen does not implement GoTo");
		}

		public virtual void HideBlockingScrim()
		{
			//also fade out the clickblocker
			if (BlockingScrim != null)
			{
				DOTween
					.To(() =>
						BlockingScrim.resolvedStyle.opacity, x => BlockingScrim.style.opacity = new StyleFloat(x), 0f, 0.5f)
					.OnComplete(() =>
					{
						BlockingScrim.parent.Remove(BlockingScrim);
					});
			}
		}

		// override to destroy costly contents when hiding. -- called during ScreenManagerTransitionOutComplete
		public virtual void TearDown()
		{
			AssetReferenceHandler?.Release();
			IsBuilt = false;
		}

		// note: build isn't called by default
		public virtual void Build()
		{
			IsBuilt = true;
		}

		public virtual ScreenData? GetData()
		{
			return null;
		}
	}

	public class Screen<T> : ScreenBase where T : ScreenData
	{
		private static uint NextInstanceId = 0;
		public T Data { get; private set; } = null!;
		public uint InstanceId;                 // might not need this value in the end, but for now using to differentiate


		public override void Awake()
		{
			var closeButton = this.Q<Button>("close-button");
			if (closeButton != null)
			{
				closeButton.clickable.clicked += OnClose;
			}
		}

		protected void OnClose()
		{
			ScreenManager.Instance.CloseScreen(this, Data.IsOverlay);
		}

		public override void Init(ScreenData data)
		{
			InstanceId = NextInstanceId++;

			var dataAsT = data as T;
			if (dataAsT == null)
			{
				Debug.LogError($"Could not cast Screen Data to {typeof(T)}! Check the screen you've launched matches the screen you expect.");
				throw new InvalidOperationException("Unknown type provided as Screen Data.");
			}

			Data = dataAsT;
			if (Data == null)
			{
				Debug.LogError($"Could not cast Screen Data!");
				throw new InvalidOperationException($"Could not reconcile Screen Data to designated type {typeof(T).Name}");
			}

			Id = Data.Id;
			CloseOnBlockingScrimClick = Data.ClickBlockerToDismiss;
			IsOverlay = Data.IsOverlay;
			TransitionIn = Data.ShowTransitionIn;
			TransitionOut = Data.ShowTransitionOut;

			//Debug.Log($"initing Screen {this.name} with id {InstanceId}");
			OnInit();
		}

		public override ScreenData? GetData()
		{
			return Data;
		}

	}
}
