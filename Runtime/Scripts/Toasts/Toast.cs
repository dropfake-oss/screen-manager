using System;
using DG.Tweening;
using UI.Core;
using UI.Tweening;
using UnityEngine;
using UnityEngine.UIElements;

namespace ScreenManagement.Toasts
{

    public class Toast : VisualElementWithSubscriptions
    {
	    public event Action ToastActionExecutedEvent;

        protected Action<Toast> hideCompleteCallback;

        // // animation properties
        protected Sequence AnimShowHide;
        protected Sequence AnimPostion;

        public virtual void StartAutoHideTimer(float duration)
        {
            DropTween.Func(() => Hide(), duration);
        }

        public virtual void Initialize(string title, string description, string? iconKey = null)
        {

        }

        public virtual Sequence Show()
        {
            return DropTween.DropTweenSequence();
        }

        public virtual Sequence Hide()
        {
            return DropTween.DropTweenSequence();
        }

        public virtual void SetPosition(VisualElement target, float scale, float position)
        {
        }

        public virtual void SetHideCompleteCallback(Action<Toast> callback)
        {
            hideCompleteCallback = callback;
        }

        public virtual void OnHideAnimationComplete()
        {
	        ToastActionExecutedEvent?.Invoke();
            hideCompleteCallback?.Invoke(this);
        }

        protected void CancelAnim(Sequence anim)
        {
            if (anim != null)
            {
                anim.Kill();
                anim = null;
            }
        }
    }
}
