using System;

namespace ScreenManagement
{
	public class HeaderData
	{
		public string? Title;
		public Action? OnBackCallback;
		public Action? OnHomeCallback;
		public bool ShowQueueLocation;
		public string State;

		public HeaderData(string state)
		{
			State = state;
		}

		public HeaderData(string state, string title, Action? onBackCallback = null)
		{
			State = state;
			Title = title;
			OnBackCallback = onBackCallback;
		}
	}
}
