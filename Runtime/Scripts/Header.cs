using System;
using UI.Core;
using UnityEngine;
using UnityEngine.UIElements;

namespace ScreenManagement
{
	public class HeaderState
	{
		public const string HIDDEN = "HIDDEN";
	}

	[UxmlElement]
	public partial class Header : VisualElementWithSubscriptions
	{
		public string CurState { get; protected set; } = HeaderState.HIDDEN;
		public HeaderData? CurHeaderData { get; protected set; }

		// old and busted
		//public virtual void SetState(string nState, HeaderData? data)
		//{
		//	Debug.Log("SetState(" + nState + ")");

		//	if (nState == CurState) return;

		//	CurState = nState;
		//	CurHeaderData = data;
		//}

		// new hottness
		public virtual void SetState(HeaderData data)
		{
			Debug.Log("SetState(" + data.State + ")");

			if (data.State == CurState) return;

			CurState = data.State;
			CurHeaderData = data;
		}
	}
}
