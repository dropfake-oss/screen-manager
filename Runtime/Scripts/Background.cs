using System;
using UI.Core;
using UnityEngine;
using UnityEngine.UIElements;

namespace ScreenManagement
{
	public class BackgroundState
	{
		public const string HIDDEN = "HIDDEN";
		public const string VISIBLE = "VISIBLE";
	}
	public class BackgroundType
	{
		public const string DEFAULT = "DEFAULT";
	}

	[UxmlElement]
	public partial class Background : VisualElementWithSubscriptions
	{
		private string CurState = BackgroundState.HIDDEN;
		private string CurType = BackgroundType.DEFAULT;

		public virtual void SetState(string nState)
		{
			if (nState == CurState) return;

			CurState = nState;
		}

		public virtual void SetType(string nType, ScreenDirection nDirection = ScreenDirection.Forward)
		{
			if (nType == CurType) return;

			CurType = nType;
		}
	}
}
