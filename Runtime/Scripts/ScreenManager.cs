//#define IS_DEBUG
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System;
using System.Text;
using System.Linq;
using UI.Tweening;
using System.Reflection;
using System.Collections;
using ScreenManagement.Toasts;

using static UI.Tweening.DropTween;

using Addressable.Utils;

namespace ScreenManagement
{
	public enum ScreenDirection
	{
		Forward,
		Backward
	}

	//this is a temporary hack. we've come up with a new scale for Panel Settings, but b/c we can't redo all the hud and screen sizing right now we'll switch panel settings as needed.
	[Serializable]
	public struct PanelValues
	{
		public int ReferenceResolutionWidth;
		public int ReferenceResolutionHeight;
		public float MatchModeRatio;
		public string[]? ApplicableScreens;
	}

	[RequireComponent(typeof(UIDocument))]
	public class ScreenManager : MonoBehaviour
	{
		[SerializeField]
		public VisualTreeAsset CustomInspectorXML;

		public class Snapshot
		{
			public string Id = null!;
			public List<ScreenData>? ScreenStackList;
			public List<ScreenData>? OverlayList;
			public List<ScreenData>? PendingList;
			public HeaderData? LastHeaderData;
			//public string? LastHeaderState;

			public Snapshot(string id)
			{
				Id = id;
			}
		}

		public static ScreenManager Instance;

		public enum ScreenStates
		{
			Idle,
			TransitionIn,
			TransitionOut,
			ShowingScreen
		}

		[Serializable]
		public struct ScreenEntry
		{
			public string Id;
			public VisualTreeAsset Screen;
		}

		[SerializeField]
		private bool UseVariablePanelValues;

		//TODO: Create a custom editor which will hide DefaultPanelValues and ResizedPanelValues if UseVariablePanelValues is false;

		[SerializeField]
		private PanelValues DefaultPanelValues;

		[SerializeField]
		private PanelValues ResizedPanelValues; // eventually this will be the default

		[SerializeField]
		//[FormerlySerializedAs("Scrim")]
		private VisualTreeAsset Scrim;

		[SerializeField]
		private float ScreenTransitionTime = 0.6F;

		[SerializeField]
		private bool LogToConsole = false;


		private VisualElement? Root;
		private VisualElement? ScreensContainer;
		private VisualElement? OverlaysContainer;
		private VisualElement? ToastContainer;

		private VisualElement? ScreenBlockingScrim;
		private Background? Background;
		private Header? Header;
		public ScreenStates CurrentState { get; private set; }
		private Dictionary<string, ScreenBase>? PreservedScreens;       //todo: do i need to preserve these on scene changes or should it clear?
		private ScreenBase? CurrentScreen; //visual of top of the stack
		private ScreenData? TransitionScreen;
		private Stack<ScreenData> ScreenStack;
		private LinkedList<ScreenData> PendingScreens;
		private List<ScreenBase> OverlayScreens;
		private string? ScreenDebugText;
		private Action? OnTransitionComplete;
		private PanelSettings? ScreenPanelSettings;
		private Dictionary<string, Snapshot> Snapshots = new Dictionary<string, Snapshot>();

		public bool HasPendingScreens => this.PendingScreens.Count > 0;

		private VisualElement? DebugContainer;
		private Label? DebugText;

		private Queue<string> Logs = new Queue<string>();
		private int LogSize = 100;
		private int LogCount = 0;

		public delegate void OnScreenChange(ScreenData screenData);
		public OnScreenChange ScreenListeners;

		public string Log
		{
			get
			{
				var log = new StringBuilder();
				// todo: this whole situation could be improved -
				// should we just use a list, any gains we get from dequeuing efficiently might lost with having to do this reverse
				var reverseLog = Logs.Reverse();
				foreach (var logItem in reverseLog)
				{
					log.AppendLine(logItem);
				}
				return log.ToString();
			}
		}

		private void AddLog(string logText, bool isError = false)
		{
			if (isError)
			{
				Debug.LogError(logText);
			}
			else if (LogToConsole)
			{
				Debug.Log("SCREEN MANAGER: " + logText);
			}

			Logs.Enqueue($"{++LogCount}: {logText}");
			while (Logs.Count > LogSize)
			{
				Logs.Dequeue();
			}
		}

		public string ActiveScreen
		{
			get
			{
				return (CurrentScreen != null) ? CurrentScreen.Id : "";
			}
		}

		//this uses screenstack instead of the current screen b/c the transition might not be complete when it's needed.
		public string TopScreenTitle
		{
			get
			{
				var success = ScreenStack.TryPeek(out ScreenData topScreenData);
				return success ? topScreenData.HeaderTitle : "";
			}
		}

		public void Awake()
		{
			if (Instance == null)
			{
				Instance = this;
				DontDestroyOnLoad(gameObject);
			}
			else
			{
				Destroy(gameObject);
				return;
			}

			//This makes the addressableManager ready when we need it.
			var addressableManager = AddressableManager.Instance;

			CurrentState = ScreenStates.Idle;

			ScreenStack = new Stack<ScreenData>();
			PendingScreens = new LinkedList<ScreenData>();
			OverlayScreens = new List<ScreenBase>();

			var uiDocument = GetComponent<UIDocument>();
			if (uiDocument == null)
			{
				Debug.LogError("UIDocument not found!");
				return;
			}

			var root = uiDocument.rootVisualElement;
			ScreenPanelSettings = uiDocument.panelSettings;

			Root = root.Q<VisualElement>("PopupContainer");
			ScreensContainer = root.Q("ScreenContainer");
			OverlaysContainer = root.Q("OverlayContainer");
			ToastContainer = root.Q("ToastContainer");
			Background = root.Q("Background").Q<Background>();
			Header = root.Q("Header")?.Q<Header>();
			CreateDebugContainer();

			// TODO: perhaps replace this with the dynamic version same as overlays use?
			ScreenBlockingScrim = root.Q<VisualElement>("BlockingScrim");

			//why doesn't this use click event, trying out for now
			//Blocker.RegisterCallback<MouseUpEvent>(OnBlockerPressed_Mouse);
			//Blocker.RegisterCallback<PointerUpEvent>(OnBlockerPressed_Pointer);

			if (ScreenBlockingScrim != null)
			{
				ScreenBlockingScrim.RegisterCallback<ClickEvent>(OnBlockerPressed_Click);
				Root.Remove(ScreenBlockingScrim);
				ScreenBlockingScrim.RemoveFromClassList("hidden");
			}

			if (PlayerPrefs.HasKey(DEBUG_SAVE_KEY))
			{
				var savedDebugSetting = PlayerPrefs.GetInt(DEBUG_SAVE_KEY);
				ShowDebugStack = savedDebugSetting == 0 ? false : true;
				//Debug.Log($"SCREENMAN: Restoring Debug preference to {ShowDebugStack}.");
			}
		}

		private VisualElement? CreateBlockingScrim()
		{
			if (Scrim != null)
			{
				var instance = Scrim.CloneTree();
				instance.style.position = Position.Absolute;
				instance.style.width = Length.Percent(100);
				instance.style.height = Length.Percent(100);
				return instance;
			}

			return null;
		}

		private void ShowScreenBlockingScrim()
		{
			//Debug.Log("SCREENMAN Showing Screen Blocking Scrim");
			if (Root != null && ScreenBlockingScrim != null)
			{
				if (!Root.Contains(ScreenBlockingScrim))
				{
					Root.Add(ScreenBlockingScrim);
					ScreenBlockingScrim.SendToBack();

					//add color to blocker and fades it in. removing for now to let individual screens handle this look
					ScreenBlockingScrim.style.opacity = 0f;

					var fadeConfig = new FadeOutConfiguration();
					fadeConfig.Speed = 0.25f;
					DropTween.FadeIn(ScreenBlockingScrim, fadeConfig);
				}
			}
		}

		private void HideScreenBlockingScrim()
		{
			//Debug.Log("SCREENMAN Hiding Screen Blocking Scrim");
			if (Root != null && ScreenBlockingScrim != null)
			{
				if (Root.Contains(ScreenBlockingScrim))
				{
					var fadeConfig = new FadeOutConfiguration();
					fadeConfig.From = ScreenBlockingScrim.style.opacity.value;
					fadeConfig.Speed = 0.15f;
					DropTween.FadeOut(ScreenBlockingScrim, fadeConfig, OnScrimFadeComplete);
				}
			}
		}

		private void OnScrimFadeComplete()
		{
			Root.Remove(ScreenBlockingScrim);
		}

		private void OnBlockerPressed_Click(ClickEvent evt)
		{
			OnBlockingScrimPressed();
		}

		private void OnBlockingScrimPressed()
		{
			if (CurrentScreen != null && CurrentScreen.CloseOnBlockingScrimClick)
			{
				GoBack();
			}
		}

		private VisualElement? GetScreenById(string id)
		{
			//Debug.Log($"get screen by id {id}");

			if (PreservedScreens != null && PreservedScreens.ContainsKey(id))
			{
				//Debug.Log($"SCREENMAN: Using Preserved Screen Instead of Cloning!");
				return PreservedScreens[id];
			}

			//Try addressable here
			// TODO: Jem - change this to async
			var templateContainer = AddressableManager.Instance?.InstantiateVisualTreeAsset(id);

			ScreenBase screenBase = templateContainer.Q<ScreenBase>();
			if (screenBase != null)
			{
				screenBase.AssetReferenceHandler = new CustomAssetReferenceHandler(() => AddressableManager.Instance?.ReleaseVisualTreeAssetInstance(id));
			}

			if (templateContainer == null)
			{
				Debug.LogError($"Screen {id} not found.");
			}

			return templateContainer;
		}

		// transfer styles from templatecontainer to the individual popup
		private void TransferStylesFromTemplate(VisualElement templateContainer, ScreenBase screenBase)
		{
			var containerStylesheets = templateContainer.styleSheets;
			for (int i = 0; i < containerStylesheets.count; i++)
			{
				screenBase.styleSheets.Add(containerStylesheets[i]);
			}
		}

		public void ShowOverlay(ScreenData data)
		{
			AddLog($"### ShowOverlay() data.Id:{data.Id} bgType:{data.BgType}");

			if (CheckForDuplicates(data))
			{
				AddLog($"Overlay {data.Id} was a duplicate and will not be shown.");
				return;
			}

			var visualElement = GetScreenById(data.Id);
			if (visualElement == null) { return; }
			data.IsOverlay = true;      // not sure there's an instance where this should be false if it's an overlay (checked upon closing)

			var screenBase = visualElement.Q<ScreenBase>();
			if (screenBase == null)
			{
				Debug.LogError("Screen type or sub-type not found in visual element!");
				return;
			}

			UpdatePanelSettings(data.Id);       //this might be problematic if another screen with different ratio is up

			if (data.PreserveOnClose)
			{
				PreserveScreen(data.Id, screenBase);
			}

			screenBase.Init(data);
			screenBase.style.position = Position.Absolute;
			OverlayScreens.Add(screenBase);
			TransferStylesFromTemplate(visualElement, screenBase);

			OverlayTransitionIn(screenBase, data.IncludeBlockingScrim);

			if (data.IncludeBlockingScrim || data.BlurBackground)
			{
				var scrim = CreateBlockingScrim();
				if (scrim != null)
				{
					scrim.name = data.Id + "_scrim";
					screenBase.BlockingScrim = scrim;
					if (data.ClickBlockerToDismiss)
					{
						scrim.RegisterCallback<ClickEvent>(clickEvent =>
						{
							if (IsShowing(data.Id))
							{
								//todo: this is gross and i don't like it.
								screenBase.Hide(() =>
								{
									CloseScreen(screenBase, true);
								}, ScreenDirection.Backward);
							}
							else
							{
								Debug.LogError("A Screen has been removed without its scrim. Investigate.");
								screenBase.HideBlockingScrim();
							}
						});
					}

					scrim.Q<BlurElement>()?.SetBlur(data.BlurBackground);

					OverlaysContainer?.Add(scrim);
					scrim.PlaceBehind(screenBase);

					var fadeConfig = new FadeInConfiguration();
					fadeConfig.Speed = 0.25f;
					DropTween.FadeIn(scrim, fadeConfig);
				}
			}

			UpdateDebugText();
		}

		// a helper method for basic overlays
		public void ShowOverlay(string screenId, bool includeBlockingScrim, bool ignoreDuplicates = false)
		{
			var data = new ScreenData(screenId);
			data.IncludeBlockingScrim = includeBlockingScrim;
			data.IgnoreDuplicates = ignoreDuplicates;
			ShowOverlay(data);
		}

		/// <summary>
		/// Returns appropriate ScreenData using reflection if found in Screens.
		/// Returns null when not found or cannot create.
		///
		/// ONLY USE FOR DEBUG MENU purpose.
		/// </summary>
		/// <param name="screenId">screenID reference from the Screens property.</param>
		/// <returns></returns>
		ScreenData? GetDebugMenuScreenData(string screenId)
		{
			VisualElement screen = GetScreenById(screenId);
			if (screen != null)
			{
				var screenBase = screen.Q<ScreenBase>();
				if (screenBase != null)
				{
					//Getting info on the screenData property. named "Data" in Screen<T>
					PropertyInfo data = screenBase.GetType().GetProperty("Data");
					if ((data != null) &&
						(data.PropertyType.IsSubclassOf(typeof(ScreenData)) || data.PropertyType == typeof(ScreenData)))
					{

						//Looking for Constructors
						ConstructorInfo[] ctors = data.PropertyType.GetConstructors();
						ConstructorInfo ctor = null;

						//Filtering the one with string screenID parameter
						foreach (var constructor in ctors)
						{
							var parameters = constructor.GetParameters();
							if (parameters.Length > 0 && parameters[0].ParameterType == typeof(string))
							{
								ctor = constructor;
								break;
							}
						}

						//If found appropriate constructor...
						if (ctor != null)
						{
							//Obtain the parameters of the constructor
							ParameterInfo[] parameters = ctor.GetParameters();

							//Create an array to hold the constructor's arguments
							object[] constructorArgs = new object[parameters.Length];

							for (int i = 0; i < parameters.Length; i++)
							{
								Type parameterType = parameters[i].ParameterType;
								if (parameterType == typeof(string))
								{
									// If string, set it to screenId
									constructorArgs[i] = screenId;
								}
								else
								{
									//Try creating object of parameterType of constructor
									try
									{
										var parameterVal = Activator.CreateInstance(parameterType);
										constructorArgs[i] = (parameterVal != null) ? parameterVal : null;
									}
									catch (Exception e)
									{
										if (e is not MissingMethodException)
										{
											//Some objects would not have default/parameterless constructors, which would
											// produce a MissingMethodException -> fallback value: null

											//This would/should only be used by debug screen instantiation.
											//Other Exceptions are logged.
											Debug.LogError("Exception Encountered : " + e.Message, gameObject);
											continue;
										}
									}
								}
							}

							// After generating parameter values, use that to create object of type ScreenData.
							var returnValue = Activator.CreateInstance(data.PropertyType, constructorArgs) as ScreenData;
							if (returnValue != null && returnValue is ScreenData)
							{
								returnValue.DummyData();
								return returnValue;
							}
						}
						else
						{
							Debug.LogError("No suitable constructor found to create ScreenData for " + data.PropertyType + ".", gameObject);
							return null;
						}
					}
				}
			}

			return null;
		}

		public void RemoveOverlay(ScreenBase overlay)
		{
			AddLog($"SM: RemoveOverlay {overlay.Id}");
			OverlaysContainer?.Remove(overlay);
			OverlayScreens.Remove(overlay);

			if (overlay.BlockingScrim != null)
			{
				var fadeConfig = new FadeOutConfiguration();
				fadeConfig.Speed = 0.25f;
				DropTween.FadeIn(overlay.BlockingScrim, fadeConfig, () =>
				{
					overlay.TearDown();
					overlay.BlockingScrim.Q<BlurElement>()?.SetBlur(false);
					// Debug.Log($"NEE REMOVE BLOCKING SCRIM {overlay.BlockingScrim} -- contains? {OverlaysContainer?.Contains(overlay.BlockingScrim)}");
					OverlaysContainer?.Remove(overlay.BlockingScrim);
				});
			}
			else
			{
				overlay.TearDown();
			}

			UpdateDebugText();
		}

		private bool CheckForDuplicates(ScreenData data)
		{
			if (!data.IgnoreDuplicates)
			{
				return false;
			}

			//this section checks the currently showing screen
			if (IsShowing(data.Id))
			{
				AddLog($"SM Ignored Show Screen because duplicates are set to ignore and {data.Id} is showing.");
				return true;
			}

			//this section checks Pending Screens...this works for our current design, but might not work for all designs going forward.
			//using this as a safeguard against duplicate (spam) clicks that occur before the screen can even show
			//todo: evaluate whether this suits out needs.
			if (data.priority == ScreenData.PriorityType.Immediate || data.priority == ScreenData.PriorityType.FrontOfQueue)
			{
				if (PendingScreens.First?.Value.Id == data.Id)
				{
					return true;
				}
			}
			else    // if priority is queue
			{
				if (PendingScreens.Last?.Value.Id == data.Id)
				{
					return true;
				}
			}

			return false;
		}

		public void ShowScreen(ScreenData data)
		{
			AddLog($"### ShowScreen() ID:{data.Id} data.BgType:{data.BgType}");

			if (CheckForDuplicates(data))
			{
				return;
			}

			if (data.IsOverlay)
			{
				ShowOverlay(data);
				return;
			}

			SetBackground(data, ScreenDirection.Forward);


			//BlurBackground for screens   //should this be here...the screen might get queued.
			if (data.BlurBackground || data.IncludeBlockingScrim)
			{
				var scrim = CreateBlockingScrim();
				scrim?.Q<BlurElement>().SetBlur(data.BlurBackground);
			}

			//Debug.Log($"SCREENMAN: Show screen {data.Id}. Currstate = {CurrentState.ToString()}");

			// all screendata is added to pending
			if (data.priority == ScreenData.PriorityType.Queue)
			{
				AddLog($"{data.Id} added to end of Pending Queue.");
				PendingScreens.AddLast(data);
			}
			else
			{
				AddLog($"{data.Id} added to front of Pending Queue.");
				PendingScreens.AddFirst(data);
			}

			if (CurrentScreen != null)
			{
				if (data.priority == ScreenData.PriorityType.Immediate)
				{
					TransitionScreen = PendingScreens.First();
					if (CurrentState == ScreenStates.Idle || CurrentState == ScreenStates.ShowingScreen)
					{
						AddLog($"Immediate priority screen has forced transition out.");
						var lastScreenData = ScreenStack.Peek();
						if (lastScreenData.PreserveOnClose)
						{
							PreserveScreen(lastScreenData.Id, CurrentScreen);
						}

						ScreenTransitionOut(CurrentScreen, ScreenDirection.Forward);
					}
					else    // previous screen is still transitioning, must wait for it to complete transition.
					{
						OnTransitionComplete = () => ShowNextInQueue();
						AddLog($"SM is in transition, cannot show {data.Id} until {CurrentScreen.Id} completes.");
					}
				}
			}
			else    // nothing else is showing, so show the item we just queued
			{
				ShowNextInQueue();
			}

			UpdateDebugText();
		}

		// a helper method for basic screen data
		public void ShowScreen(string screenId, ScreenData.PriorityType priority = ScreenData.PriorityType.Queue, bool includeBlockingScrim = true)
		{
			var data = GetDebugMenuScreenData(screenId); // This call
			if (data == null) { return; }

			data.IncludeBlockingScrim = includeBlockingScrim;
			data.priority = priority;

			ShowScreen(data);
		}

		public void ReplaceScreen(ScreenData data)
		{
			//Debug.Log($"SCREENMAN: Replace screen {data.Id}.");
			PendingScreens.AddFirst(data);      // add to front to skip the line
			if (CurrentScreen != null)
			{
				var prevData = ScreenStack.Pop();
				if (prevData.PreserveOnClose)
				{
					PreserveScreen(prevData.Id, CurrentScreen);
				}
				ScreenTransitionOut(CurrentScreen, ScreenDirection.Backward);
			}
			else
			{
				ShowNextInQueue();
			}

			UpdateDebugText();
		}

		public void CloseScreen(string screenId)
		{
			AddLog($"### ScreenManager - CloseScreen()");

			foreach (var overlay in OverlayScreens)
			{
				if (overlay.Id == screenId)
				{
					RemoveOverlay(overlay);
					return;
				}
			}

			if (CurrentScreen != null)
			{
				if (CurrentScreen.Id == screenId)
				{
					GoBack();
					return;
				}
			}

			AddLog($"SM Could not find open screen matching id {screenId} to close", true);
		}

		public void CloseScreen(VisualElement screen, bool isOverlay = false)
		{
			//do we care if this screen is on the top?? or should we close it regardless if we get the message to do so?
			//Debug.Log("SCREENMAN: close screen " + screen);
			if (isOverlay)
			{
				foreach (var overlay in OverlayScreens)
				{
					if (overlay == screen)
					{
						RemoveOverlay(overlay);
						return;
					}
				}
				Debug.LogError($"Could not close overlay {screen.name}");
			}
			else if (CurrentScreen != null)
			{
				if (CurrentScreen == screen)
				{
					GoBack();
				}
				else
				{
					Debug.LogError($"Cannot close..current screen {CurrentScreen} does not match.");
				}
			}
		}

		public void CloseOverlay(string overlayId)
		{
			AddLog($"User called CloseOverlay for {overlayId}");
			foreach (ScreenBase overlay in OverlayScreens)
			{
				if (overlay.Id == overlayId)
				{
					overlay.Hide(() =>
					{
						RemoveOverlay(overlay);
					}, ScreenDirection.Backward);
					return;
				}
			}
			AddLog($"Overlay {overlayId} not found!");
		}

		public void ReplaceOverlay(string closedOverlayId, ScreenData nextOverlayData)
		{
			AddLog($"User called ReplaceOverlay to swap {closedOverlayId} for {nextOverlayData.Id}");
			foreach (ScreenBase overlay in OverlayScreens)
			{
				if (overlay.Id == closedOverlayId)
				{
					overlay.Hide(() =>
					{
						RemoveOverlay(overlay);
						ShowOverlay(nextOverlayData);
					}, ScreenDirection.Backward);
					return;
				}
			}
			AddLog($"Overlay {closedOverlayId} not found!");
			ShowOverlay(nextOverlayData);
		}

		public bool IsShowing(string screenName)
		{
			// check overlays
			foreach (ScreenBase overlay in OverlayScreens)
			{
				if (overlay.Id == screenName) { return true; }
			}

			if (TransitionScreen != null && TransitionScreen.Id == screenName)
			{
				return true;
			}

			// then check the stack
			if (CurrentScreen != null)
			{
				return CurrentScreen.Id == screenName;
			}


			return false;
		}

		public bool StackContains(string screenName)
		{
			//then check the stack
			if (ScreenStack != null)
			{
				return ScreenStack.Any(s => s.Id == screenName);
			}

			return false;
		}

		public void TryGoBack()
		{
			if (ScreenStack.Count > 0)
			{
				GoBack();
			}
		}

		public string GoBack()
		{
			var nextScreen = "";
			//Debug.Log(ScreenStack.Count == 0 ? "SCREENMAN:  - Screen Stack is Empty!" : "SCREENMAN:  - Going back");

			if (CurrentScreen != null)
			{
				var data = ScreenStack.Pop();
				if (data.PreserveOnClose)
				{
					PreserveScreen(data.Id, CurrentScreen);
				}

				if (ScreenStack.TryPeek(out ScreenData nextScreenData))
				{
					nextScreen = nextScreenData.Id;
				}

				ScreenTransitionOut(CurrentScreen, ScreenDirection.Backward);
			}

			if (ScreenStack.Count == 0)
			{
				HideScreenBlockingScrim();
			}

			UpdateDebugText();
			return nextScreen;
		}

		private void PreserveScreen(string screenId, ScreenBase screen)
		{
			if (PreservedScreens == null)
			{
				PreservedScreens = new Dictionary<string, ScreenBase>();
			}

			if (PreservedScreens.ContainsKey(screenId))
			{
				PreservedScreens[screenId] = screen;
			}
			else
			{
				PreservedScreens.Add(screenId, screen);
			}

			AddLog($"SM Preserving Screen {screenId}");
		}

		// For SequenceScreens, show the next in a screen's sequence if there is one
		public void Next()
		{
			CurrentScreen?.Next();
		}

		// For SequenceScreens, go to a specific index in a screen's sequence
		public void GoTo(int index)
		{
			CurrentScreen?.GoTo(index);
		}

		private void ScreenTransitionIn(VisualElement screen, ScreenDirection? direction, bool includeBlocker = true)
		{
			AddLog($"SM transitioning in screen {screen.name}");
			SetState(ScreenStates.TransitionIn);

			if (CurrentScreen == null)
			{
				AddLog("SM cannot transition In -- current screen is null");
				return;
			}

			if (includeBlocker)
			{
				ShowScreenBlockingScrim();
			}

			ScreensContainer?.Add(screen);
			UpdatePanelSettings(CurrentScreen.Id);

			if (CurrentScreen.TransitionIn)
			{
				ScreenBase? sb = screen as ScreenBase;
				sb?.Show(ScreenTransitionInComplete, direction);
			}
			else
			{
				screen.style.top = 0; //necessary?
				ScreenTransitionInComplete();
			}
		}

		private void ScreenTransitionInComplete()
		{
			AddLog($"SM Transition In Complete");

			SetState(ScreenStates.ShowingScreen);

			//might need to play with positioning of this
			if (ScreenStack.TryPeek(out ScreenData currentScreenData))
			{
				if (ScreenListeners != null)
				{
					ScreenListeners(currentScreenData);
				}
				AddLog($"SM invoking {currentScreenData.Id} show callback");
				currentScreenData.ShowCallback?.Invoke();
			}

			if (OnTransitionComplete != null)
			{
				if (CurrentScreen != null)
				{
					ScreensContainer?.Remove(CurrentScreen);    //what is this doing?
				}
				OnTransitionComplete();
				OnTransitionComplete = null;
			}
		}

		private void ScreenTransitionOut(ScreenBase screen, ScreenDirection? direction = null)
		{
			AddLog($"SM ScreenTransitionOut {screen}");
			SetState(ScreenStates.TransitionOut);

			if (screen.TransitionOut)
			{
				ScreenBase sb = screen as ScreenBase;
				sb.Hide(() => ScreenTransitionOutComplete(screen), direction);
			}
			else
			{
				ScreenTransitionOutComplete(screen);
			}
		}

		private void ScreenTransitionOutComplete(ScreenBase screen)
		{
			AddLog($"SM Overlay Transition Out Complete. {screen}");
			ScreensContainer?.Remove(screen);
			SetState(ScreenStates.Idle);
			CurrentScreen = null;

			screen.TearDown();

			//might need to play with positioning of this
			//Demute : Getting from the screen stack here doesn't work as expected,
			//cause the screen transitioning out is already removed from the stack in case we press on back/home.
			// if (ScreenStack.TryPeek(out ScreenData currentScreenData))
			// {
			// 	AddLog($"SM invoking {currentScreenData.Id} hide callback");
			// 	currentScreenData.HideCallback?.Invoke();
			// }

			ScreenData? hiddenScreenData = screen.GetData();
			if (hiddenScreenData != null)
			{
				AddLog($"SM invoking {hiddenScreenData.Id} hide callback");
				hiddenScreenData.HideCallback?.Invoke();
			}

			if (OnTransitionComplete != null)
			{
				OnTransitionComplete();
				OnTransitionComplete = null;
			}
			else
			{
				ShowNextInQueue(ScreenDirection.Backward);
			}

		}

		private void OverlayTransitionIn(VisualElement overlay, bool includeBlocker = true)
		{
			OverlaysContainer?.Add(overlay);
			ScreenBase? sb = overlay as ScreenBase;
			sb?.Show(null, ScreenDirection.Forward); //overlay will always be forward??
			AddLog("SM Overlay Transition In.");
		}

		private bool TryDequeuePendingScreen(out ScreenData? data)
		{
			data = null;

			if (PendingScreens.Count > 0)
			{
				data = PendingScreens.First.Value;
				PendingScreens.RemoveFirst();
				return true;
			}

			return false;
		}

		private void ShowNextInQueue(ScreenDirection direction = ScreenDirection.Forward)
		{
			AddLog($"SM Showing next in queue. Current queue count: {PendingScreens.Count}");
			if (CurrentState == ScreenStates.Idle || CurrentState == ScreenStates.ShowingScreen)
			{
				if (TryDequeuePendingScreen(out ScreenData? nextPending))
				{
					if (nextPending == null)
					{
						return;
					}

					var screen = SetCurrentScreen(nextPending);
					if (screen == null)
					{
						Debug.LogError("Screen type or sub-type not found in visual element!");
						ShowNextInQueue(direction);
						return;
					}

					//var visualElement = GetScreenById(nextPending.Id);
					//if (visualElement == null) { return; }

					//var screen = visualElement.Q<ScreenBase>();
					//if (screen == null)
					//{
					//	Debug.LogError("Screen type or sub-type not found in visual element!");
					//	ShowNextInQueue(direction);
					//	return;
					//}

					//screen.Init(nextPending);
					//ScreenStack.Push(nextPending);
					//CurrentScreen = screen;

					//TransferStylesFromTemplate(visualElement, screen);

					SetBackground(nextPending, ScreenDirection.Forward);
					ScreenTransitionIn(screen, ScreenDirection.Forward, nextPending.IncludeBlockingScrim);  //is it fair to assume this will be forward??
				}
				else    // return to the stack
				{
					AddLog("SM: Queue empty, returning to the stack");
					if (ScreenStack.TryPeek(out ScreenData prevScreenData))
					{
						var screen = SetCurrentScreen(prevScreenData, false);
						if (screen == null)
						{
							Debug.LogError("Warning: popping the invalid screen off the stack!");
							ScreenStack.Pop();
							ShowNextInQueue(direction);
							return;
						}
						SetBackground(prevScreenData, direction);
						ScreenTransitionIn(screen, direction);
					}
				}
			}
			UpdateDebugText();
		}

		private ScreenBase? SetCurrentScreen(ScreenData screenData, bool addToStack = true)
		{
			TransitionScreen = null;

			var visualElement = GetScreenById(screenData.Id);
			if (visualElement == null) { return null; }

			var screen = visualElement.Q<ScreenBase>();
			if (screen == null)
			{
				Debug.LogError("Screen type or sub-type not found in visual element!");
				return null;
			}

			screen.Init(screenData);
			if (addToStack)
			{
				ScreenStack.Push(screenData);
			}

			CurrentScreen = screen;
			AddLog($"Current Screen is now {screenData.Id}");

			TransferStylesFromTemplate(visualElement, screen);

			// might need to move this elsewhere if sequencing is weird
			if (screenData.HeaderState != null)
			{
				SetHeaderState(new HeaderData(screenData.HeaderState, screenData.HeaderTitle, screenData.OnBackOverride));
			}

			return screen;
		}

		private void SetState(ScreenStates nextState)
		{
			AddLog($"SM: Set State {nextState.ToString()}");
			CurrentState = nextState;
			UpdateDebugText();
		}

		public void ClearToScreen(string screenId, Action? onCompleteCallback = null)
		{
			AddLog($"User Called SM::ClearToScreen {screenId}");

			if (!ScreenStack.Any(screen => screen.Id == screenId))
			{
				AddLog($"SM: The specified screen {screenId} is not on the stack!", true);
				ClearToRoot();
				return;
			}

			while (ScreenStack.Peek().Id != screenId)
			{
				var screen = ScreenStack.Pop();
				AddLog($"SM: Clearing {screen.Id} from stack.");
				UpdateDebugText();
			}

			var includeCurrent = CurrentScreen.Id != screenId;
			if (onCompleteCallback == null)
			{
				if (CurrentScreen != null && includeCurrent)
				{
					ScreenTransitionOut(CurrentScreen, ScreenDirection.Backward);
				}
			}
			else
			{
				StartCoroutine(WaitUntilTransitionsComplete(includeCurrent, onCompleteCallback));
			}

			UpdateDebugText();
		}

		//clearing to a screen without having to show that screen, and instead showing a given screen
		public void ClearToScreenWith(string screenId, ScreenData nextScreenData)
		{
			AddLog($"User Called SM:ClearToScreenWith {screenId}");
			if (!ScreenStack.Any(screen => screen.Id == screenId))
			{
				AddLog($"SM: The specified screen {screenId} is not on the stack!, true");
				return;
			}

			//clear all the screens that aren't the target screen
			while (ScreenStack.Peek().Id != screenId)
			{
				var screen = ScreenStack.Pop();
				UpdateDebugText();
			}

			//now we have the screen at the top of the stack -- IF that data is immediate -- TODO: what should we do if it's not, change priority?
			ShowScreen(nextScreenData);

			if (CurrentScreen != null)
			{
				ScreenTransitionOut(CurrentScreen, ScreenDirection.Forward);
			}

			UpdateDebugText();
		}

		//a non-debug function for clearing all screens (for when a scene load is imminent, for example)
		public void ClearToRoot(Action? onCompleteCallback = null)
		{
			AddLog($"User Called SM:ClearToRoot");
			ScreenStack.Clear();
			PendingScreens.Clear();

			StartCoroutine(WaitUntilTransitionsComplete(true, onCompleteCallback));

			//TODO: SHOULD WE REJECT TO ANY NEW INCOMING SCREENS WHILE WE'RE DOING THIS?
		}

		private IEnumerator WaitUntilTransitionsComplete(bool includeCurrent, Action? onCompleteCallback = null)
		{
			int count = (CurrentScreen != null && includeCurrent ? 1 : 0) + OverlayScreens.Count;
			AddLog($"SM waiting on {count} screens to close");
			if (CurrentScreen != null)
			{
				OnTransitionComplete = () =>
				{
					count--;
				};
				ScreenTransitionOut(CurrentScreen, ScreenDirection.Backward);
			}

			foreach (ScreenBase overlay in OverlayScreens)
			{
				overlay.Hide(() =>
				{
					count--;
					RemoveOverlay(overlay);
				}, ScreenDirection.Backward);
			}

			while (count > 0)
			{
				yield return null;
			}

			HideScreenBlockingScrim();

			AddLog($"SM WaitUntilTransitionsComplete is complete. ScreenStack: {ScreenStack.Count} Pending: {PendingScreens.Count} Overlays: {OverlayScreens.Count}");
			onCompleteCallback?.Invoke();
		}

		public Action ShowBusyPopup()
		{
			var data = new ScreenData("busyPopup");
			data.IsOverlay = true;
			data.priority = ScreenData.PriorityType.Immediate;
			data.ClickBlockerToDismiss = false;
			data.ShowTransitionIn = false;
			data.ShowTransitionOut = false;
			ShowScreen(data);
			return HideBusyModal;
		}

		private void HideBusyModal()
		{
			CloseScreen("busyPopup");
		}

		public void SwapToDefaultPanelSettings()
		{
			UpdatePanelSettings("default");
		}

		// HACK to switch between two panel settings until all screens can be updated to the resized values
		private void UpdatePanelSettings(string screenName)
		{
			if (!UseVariablePanelValues) { return; }

			if (ResizedPanelValues.ApplicableScreens == null)
			{
				AddLog("ReizedPanelValues has no applicable screens listed!", true);
				return;
			}

			if (ScreenPanelSettings == null)
			{
				AddLog("ScreenPanelSettings cannot be updated as it is null!", true);
				return;
			}

			if (Array.Exists(ResizedPanelValues.ApplicableScreens, screen => screen == screenName))
			{
				AddLog("SM Updating Panel Settings to Resized");
				ScreenPanelSettings.referenceResolution = new Vector2Int(ResizedPanelValues.ReferenceResolutionWidth, ResizedPanelValues.ReferenceResolutionHeight);
				ScreenPanelSettings.match = ResizedPanelValues.MatchModeRatio;
			}
			else
			{
				AddLog("SM Updating Panel Settings to Default");
				ScreenPanelSettings.referenceResolution = new Vector2Int(DefaultPanelValues.ReferenceResolutionWidth, DefaultPanelValues.ReferenceResolutionHeight);
				ScreenPanelSettings.match = DefaultPanelValues.MatchModeRatio;
			}
		}

		public void SetBackgroundType(string bgType, ScreenDirection nDirection = ScreenDirection.Forward)
		{
			if (Background == null) { Debug.LogError("No background found!"); return; }

			Background.SetType(bgType, nDirection);

			AddLog($"SM Setting Background Type to {bgType}");
		}

		public void SetBackground(ScreenData nData, ScreenDirection nDirection = ScreenDirection.Forward)
		{
			// load in a screen background if needed
			if (nData.BgType != null)
			{
				if (Background != null) Background.SetType(nData.BgType, nDirection);
			}
		}

		#region HEADER
		public void SetHeaderState(string state)
		{
			SetHeaderState(new HeaderData(state));
		}

		public void SetHeaderState(HeaderData data)
		{
			if (Header == null) { Debug.LogError("No header found!"); return; }

			Header.SetState(data);
			AddLog($"SM Setting HeaderState to {data.State}");
		}

		public void HideHeader()
		{
			SetHeaderState(HeaderState.HIDDEN);
			AddLog($"SM Setting HeaderState to {HeaderState.HIDDEN}");
		}
		#endregion

		/**
		 * Populates the Screen Stack with Data that will NOT be shown immediately. Used for scene transitions, mostly
		 */
		public void PopulateScreenStack(params ScreenData[] screenData)
		{
			foreach (ScreenData data in screenData)
			{
				if (CheckForDuplicates(data))
				{
					continue;
				}

				if (data.IsOverlay)
				{
					ShowOverlay(data); //should we show it? or just warn user they're using this function incorrectly?
					continue;
				}

				//if (data.priority == ScreenData.PriorityType.Queue)

				// because we're adding things directly to the stack here, i don't think we can do anything about the priority. If things are already in the stack, your new items will be on top of them. Also the order you add things to this function matters.
				ScreenStack.Push(data);
			}
		}

		#region SNAPSHOTS
		public string TakeSnapshot()
		{
			string id;
			do
			{
				id = Guid.NewGuid().ToString();
			} while (Snapshots.ContainsKey(id));    //get unique id

			var snapshot = new Snapshot(id);
			snapshot.ScreenStackList = ScreenStack.ToList();
			snapshot.ScreenStackList.Reverse();
			snapshot.PendingList = PendingScreens.ToList();

			if (OverlayScreens.Count > 0)
			{
				snapshot.OverlayList = new List<ScreenData>();
				foreach (var screen in OverlayScreens)
				{
					var data = screen.GetData();
					if (data == null)
					{
						Debug.LogError("Could not retrieve screen data from overlay!");
						continue;
					}
					snapshot.OverlayList.Add(data);
					// Debug.Log($"NEE: retrieved data is {data}");
				}
			}

			snapshot.LastHeaderData = Header?.CurHeaderData;
			//snapshot.LastHeaderState = Header?.CurState;

			AddLog($"SM: Header state is {Header?.CurState} with Title {Header?.CurHeaderData?.Title}");

			Snapshots.Add(id, snapshot);
			AddLog($"SM took a snapshot with id: {id}");
			return id;
		}

		public void DeleteSnapshot(string id)
		{
			AddLog($"User requested deletion of snapshot with id: {id}");
			Snapshots.Remove(id);
		}

		public void ApplySnapshot(string id, bool keepSnapshot = false, bool clearExistingScreens = true)
		{
			AddLog($"User requested snapshot with id: {id}");
			if (!Snapshots.ContainsKey(id))
			{
				AddLog($"Snapshot {id} not found", true);
				return;
			}

			if (clearExistingScreens)
			{
				ClearToRoot(() => SetSnapshotData(id, keepSnapshot));
			}
			else
			{
				SetSnapshotData(id, keepSnapshot);
			}
		}

		private bool SetSnapshotData(string id, bool keepSnapshot)
		{
			if (!Snapshots.TryGetValue(id, out var snapshot)) { return false; }

			if (snapshot.ScreenStackList != null)
			{
				for (var i = 0; i < snapshot.ScreenStackList.Count - 1; i++)
				{
					AddLog($"SM: Snapshot pushing to stack {snapshot.ScreenStackList[i].Id}");
					ScreenStack.Push(snapshot.ScreenStackList[i]);
				}
				//and show the last one
				ShowScreen(snapshot.ScreenStackList[snapshot.ScreenStackList.Count - 1]);
			}

			if (snapshot.OverlayList != null)
			{
				foreach (var overlayData in snapshot.OverlayList)
				{
					AddLog($"SM: Snapshot adding overlay {overlayData.Id}");
					ShowOverlay(overlayData);
				}
			}

			if (snapshot.PendingList != null)
			{
				foreach (var pendingData in snapshot.PendingList)
				{
					AddLog($"SM: Snapshot pushing to pending {pendingData.Id}");
					PendingScreens.AddLast(pendingData);
				}
			}

			if (snapshot.LastHeaderData != null)
			{
				AddLog($"SM: Snapshot setting header to state {snapshot.LastHeaderData.State} with title {snapshot.LastHeaderData.Title}");
				SetHeaderState(snapshot.LastHeaderData);
			}

			if (!keepSnapshot)
			{
				AddLog($"SM: Deleting Snapshot {id}");
				Snapshots.Remove(id);
			}

			UpdateDebugText();
			return true;
		}
		#endregion

		#region DEBUG
		//TODO: improve the visibility handling of this debug menu
		public void Update()
		{
			//if (ShowDebugStack && (DebugContainer == null || !DebugContainer.visible))
			if (DebugContainer != null && DebugContainer.visible != ShowDebugStack)
			{
				ToggleDebugView(ShowDebugStack);
			}

			if (ShowDebugStack && DebugText != null)
			{
				DebugText.text = ScreenDebugText;
			}
		}

		public void ToggleDebugView(bool showDebug)
		{
			ShowDebugStack = showDebug;

			if (DebugContainer == null) { return; }
			DebugContainer.visible = showDebug;
			//PlayerPrefs.SetInt(DEBUG_SAVE_KEY, showDebug ? 1 : 0);
			//Debug.Log($"SCREENMAN SAVING DEBUG KEY TO {PlayerPrefs.GetInt(DEBUG_SAVE_KEY)}");
		}

		[SerializeField]
		private bool ShowDebugStack = true;
		public string DEBUG_SAVE_KEY = "ScreenManagerShowDebug";
		private Button DebugClose;

		private Button GetDebugButton(string buttonText)
		{
			var button = new Button();
			button.text = buttonText;
			button.style.borderBottomWidth = 0;
			button.style.borderLeftWidth = 0;
			button.style.borderRightWidth = 0;
			button.style.borderTopWidth = 0;
			button.style.paddingBottom = 1;
			button.style.paddingTop = 1;
			button.style.paddingRight = 1;
			button.style.paddingLeft = 1;
			return button;
		}

		private void CreateDebugContainer()
		{
			if (Debug.isDebugBuild)
			{
				var uiDocument = GetComponent<UIDocument>();
				if (uiDocument == null)
				{
					Debug.LogError("UIDocument not found!");
					return;
				}

				var root = uiDocument.rootVisualElement;

				DebugContainer = new VisualElement();
				DebugContainer.style.position = Position.Absolute;
				DebugContainer.style.flexGrow = 1;
				DebugContainer.style.backgroundColor = new Color(0, 0, 0, 0.5f);
				DebugContainer.style.bottom = 0;
				DebugContainer.style.fontSize = 10;
				DebugContainer.pickingMode = PickingMode.Ignore;
				DebugContainer.name = "ScreenManagerDebug";

				DebugText = new Label();
				DebugText.style.color = Color.cyan;
				DebugText.pickingMode = PickingMode.Ignore;
				//DebugText.style.unityFontStyleAndWeight = FontStyle.Bold;
				//DebugText.style.fontSize = 12;
				DebugContainer.Add(DebugText);

				var buttonPanel = new VisualElement();
				buttonPanel.style.justifyContent = Justify.SpaceBetween;
				buttonPanel.name = "DebugButtons";
				buttonPanel.style.flexDirection = FlexDirection.Row;
				DebugContainer.Add(buttonPanel);

				DebugClose = GetDebugButton("Close Screen");
				DebugClose.style.flexGrow = 1;
				DebugClose.style.height = 16;
				DebugClose.clicked += () =>
				{
					if (CurrentScreen != null) { CloseScreen(CurrentScreen); }
				};
				buttonPanel.Add(DebugClose);

				var toggleBtn = GetDebugButton("x");
				toggleBtn.style.width = 16;
				toggleBtn.style.height = 16;
				toggleBtn.clicked += () =>
				{
					ToggleDebugView(false);
				};
				buttonPanel.Add(toggleBtn);

				root.Add(DebugContainer);

				DebugContainer.visible = false; //default condition
			}
		}

		private void UpdateDebugText()
		{
			if (ShowDebugStack && DebugContainer == null)
			{
				CreateDebugContainer();
			}

			if (DebugClose != null)
			{
				DebugClose.style.display = CurrentScreen == null ? DisplayStyle.None : DisplayStyle.Flex;
			}

			var hasContent = ScreenStack.Count > 0 || PendingScreens.Count > 0 || OverlayScreens.Count > 0;

			// this doesn't work yet b/c update turns it back on, but if we can get rid of the update for toggle put this back in
			//DebugContainer.visible = ShowDebugStack && hasContent;

			//Debug.Log($"SCREENMAN: UpdateDebugText {DebugContainer.visible} -- ShowDebug {ShowDebugStack}, hasContent {hasContent}");

			if (!hasContent)
			{
				ScreenDebugText = "";
				return;
			}

			StringBuilder sb = new StringBuilder();
			sb.AppendLine("Stack:");
			foreach (ScreenData screen in ScreenStack)
			{
				sb.AppendLine(screen.Id);
			}

			sb.AppendLine("\nQueued:");
			foreach (var screenData in PendingScreens)
			{
				sb.AppendLine(screenData.Id);
			}

			sb.AppendLine("\nOverlays:");
			foreach (ScreenBase screen in OverlayScreens)
			{
				sb.AppendLine(screen.Id);
			}

			if (CurrentScreen != null)
			{
				//sb.AppendLine("\nCurrent: " + CurrentScreen.ToString().Substring(0, 12));
				sb.AppendLine("\nCurrent: " + CurrentScreen.GetData()?.Id);
			}

			if (TransitionScreen != null)
			{
				sb.AppendLine("\nTransition: " + TransitionScreen.Id);
			}
			sb.AppendLine("\nState: " + CurrentState.ToString());


			ScreenDebugText = sb.ToString();
		}

		// a debug function for clearing all out screens in test environment
		public void ClearStackandQueue()
		{
			ScreenStack.Clear();
			PendingScreens.Clear();
			OverlayScreens.Clear();
			ScreensContainer?.Clear();
			OverlaysContainer?.Clear();
			CurrentScreen = null;

			SetState(ScreenStates.Idle);
			HideScreenBlockingScrim();
			UpdateDebugText();
		}

		#endregion DEBUG
	}
}
