using System.Linq;
using UI.Core;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UIElements;

namespace ScreenManagement
{
	[UxmlElement]
	public partial class BlurElement : VisualElementWithSubscriptions
	{
		private Camera _camera;
		private Shader? _blurShader;
		private Material _blurMaterial;
		private RenderTexture _blurRT;
		private CommandBuffer? _commandBuffer;
		private CameraEvent _cameraInjectEvent;
		RenderTextureDescriptor _rtDesc = new RenderTextureDescriptor(Screen.width / 8, Screen.height / 8, RenderTextureFormat.ARGB32, 0);
		[SerializeField, Range(1, 10)]
		private int _blurIterations = 3;

		public override void Awake()
		{
			// if (!IsInitialized) { Initialize(); } //i don't want this component doing work if it's not in use. - ne
		}

		public void SetBlur(bool value)
		{
			if (value)
			{
				if (!IsInitialized) { Initialize(); }
				if (IsInitialized)
				{
					CreateBuffer();
					style.display = DisplayStyle.Flex;
				}
			}
			else
			{
				Cleanup();
				style.display = DisplayStyle.None;
			}
		}

		private void Initialize()
		{
			//Set Size
			style.width = new StyleLength(Length.Percent(100));
			style.height = new StyleLength(Length.Percent(100));
			if (!Application.isPlaying) { return; }
			IsInitialized = LoadResources();
			style.display = DisplayStyle.None; // hidden by default
		}

		bool LoadResources()
		{
			_camera = Camera.allCameras.ToList().Where(x => (x.enabled && x.tag == "MainCamera")).FirstOrDefault();
			if (!_camera)
			{
				Debug.LogWarning("ScreenManager -> UI BlurElement | No active camera found to blur.");
				return false;
			}
			_blurShader = GetBlurShader();
			_blurRT = new RenderTexture(_rtDesc);
			if (_blurShader != null)
			{
				if (!_blurMaterial)
				{
					_blurMaterial = new Material(_blurShader);
					_blurMaterial.hideFlags = HideFlags.HideAndDontSave;
				}
				_cameraInjectEvent = CameraEvent.AfterSkybox;
				this.style.backgroundImage = UnityEngine.UIElements.Background.FromRenderTexture(_blurRT);
				return true;
			}
			else
			{
				Debug.LogWarning("ScreenManager -> UI BlurElement | Can't load Blur Shader for background blur.");
				return false;
			}
		}

		void CreateBuffer()
		{
			_commandBuffer = null;
			_commandBuffer = new CommandBuffer();
			_commandBuffer.name = "UIBackgroundBlur";
			RenderTexture tempRT = new RenderTexture(_rtDesc);
			RenderTexture tempRT2 = new RenderTexture(_rtDesc);
			_commandBuffer.Blit(BuiltinRenderTextureType.CurrentActive, tempRT);
			for (int i = 0; i < _blurIterations; i++)
			{
				// horizontal blur
				_commandBuffer.SetGlobalVector("offsets", new Vector4(4.0f / Screen.width, 0, 0, 0));
				_commandBuffer.Blit(tempRT, tempRT2, _blurMaterial);
				// vertical blur
				_commandBuffer.SetGlobalVector("offsets", new Vector4(0, 4.0f / Screen.height, 0, 0));
				_commandBuffer.Blit(tempRT2, tempRT, _blurMaterial);
				// horizontal blur
				_commandBuffer.SetGlobalVector("offsets", new Vector4(4.0f / Screen.width, 0, 0, 0));
				_commandBuffer.Blit(tempRT, tempRT2, _blurMaterial);
				// vertical blur
				_commandBuffer.SetGlobalVector("offsets", new Vector4(0, 4.0f / Screen.height, 0, 0));
				_commandBuffer.Blit(tempRT2, tempRT, _blurMaterial);
			}
			tempRT2.Release();
			_commandBuffer.Blit(tempRT, _blurRT);
			tempRT.Release();
			_camera.AddCommandBuffer(_cameraInjectEvent, _commandBuffer);
		}

		public override void OnDestroy()
		{
			Cleanup();
		}

		private void Cleanup()
		{
			_camera?.RemoveCommandBuffers(_cameraInjectEvent);
			_blurRT?.Release();
			_blurMaterial = null;
		}

		private Shader GetBlurShader()
		{
			// Create a new shader from the code
			Shader blurShader = Resources.Load<Shader>("UIBlurShader");
			return blurShader;
		}
	}
}
