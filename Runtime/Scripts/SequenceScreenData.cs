﻿using System;

namespace ScreenManagement
{
	public class SequenceScreenData : ScreenData
	{
		public bool LoopSequence = false;
		public bool AutoClose = false; // note: auto close will never trigger if Loop Sequence is true
		public bool AutoAdvance = false;
		public int AdvanceIntervalInMS = 4000;
		public int StartingIndex;

		public SequenceScreenData(string id) : base(id)
		{
		}
	}
}

